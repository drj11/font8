package main

import (
	"encoding/binary"
	"io"
)

type State struct {
	major, minor int
	machineId    int
	flag         int
	block        <-chan Block
}

type Block struct {
	id    []byte
	size  int32
	block []byte
}

// See http://www.spectaculator.com/docs/zx-state/intro.shtml
func openSZX(r io.Reader) *State {
	state := State{}

	header := make([]byte, 8)
	r.Read(header)

	state.major = int(header[4])
	state.minor = int(header[5])
	state.machineId = int(header[6])
	state.flag = int(header[7])

	block_chan := make(chan Block)
	state.block = block_chan

	go func() {
		for {
			bs := Block{}
			block_id := make([]byte, 4)
			n, err := r.Read(block_id)
			if n == 0 {
				break
			}
			_ = err
			bs.id = block_id

			binary.Read(r, binary.LittleEndian, &bs.size)
			bs.block = make([]byte, bs.size)
			r.Read(bs.block)
			block_chan <- bs
		}
		close(block_chan)
	}()
	return &state
}
