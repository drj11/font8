package main

import (
	"bytes"
	"compress/zlib"
	"encoding/binary"
	"flag"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

var offset = flag.Int("offset", 0, "Extract up to 96 glyphs from given byte offset")

func main() {
	flag.Parse()
	snap_name := flag.Arg(0)

	// Open the snapshot file.
	r, err := os.Open(snap_name)
	if err != nil {
		log.Fatal(err)
	}

	state := openSZX(r)
	fmt.Println(state)

	// Collect all the RAM PAGE blocks (block id "RAMP").
	ram := Ram{}
	fmt.Print("RAMPAGE")
	for block := range state.block {
		id := string(block.id)
		if id == "RAMP" {
			page := ram.Add(block)
			fmt.Printf(" %d", page)
		}
	}
	fmt.Println()

	linear := ram.Linear()
	fmt.Println(len(linear))
	raw := bytes.NewReader(linear)

	if *offset == 0 {
		WriteEntirePNG(snap_name, raw)
	} else {
		WriteFontPNG(snap_name, raw, *offset)
	}
}

// Write the entire linear RAM out into a (768x512) PNG image.
// Name of output file is derived from base_name, by replacing
// extension with ".png".
func WriteEntirePNG(base_name string, raw io.Reader) {

	png_name := strings.TrimSuffix(base_name, filepath.Ext(base_name)) + ".png"
	w, err := os.OpenFile(png_name, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer w.Close()

	rawToPNG(raw, w)
}

// Write up to 96 8x8 glyphs out into a 256x24 PNG image.
func WriteFontPNG(base_name string, raw io.ReadSeeker, offset int) {
	png_name := strings.TrimSuffix(base_name,
		filepath.Ext(base_name)) + "-font.png"
	w, err := os.OpenFile(png_name, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer w.Close()

	raw.Seek(int64(offset), io.SeekStart)

	fontToPNG(raw, w)
}

type Ram struct {
	page map[int][]byte
}

func (r *Ram) Add(b Block) int {
	if string(b.id) != "RAMP" {
		log.Fatal("RAM to add must be a RAMP Block; is", b.id)
	}
	flag := binary.LittleEndian.Uint16(b.block[:2])

	page := int(b.block[2])

	addend := []byte{}
	if flag&1 == 0 {
		addend = b.block[3:]
	} else {
		br := bytes.NewReader(b.block[3:])
		reader, err := zlib.NewReader(br)
		defer reader.Close()
		addend, err = ioutil.ReadAll(reader)
		if err != nil {
			log.Fatal("zlib error", err)
		}
	}

	if r.page == nil {
		r.page = map[int][]byte{}
	}
	r.page[page] = addend
	return page
}

func (r *Ram) Linear() []byte {
	// The page numbers are tolerably well documented
	// here: https://faqwiki.zxnet.co.uk/wiki/ZX_Spectrum_128
	// But the summary for our purposes is:
	//   5: 0x4000,
	//   2: 0x8000,
	//   0: 0xc000,

	result := []byte{}
	result = append(result, r.page[5]...)
	result = append(result, r.page[2]...)
	result = append(result, r.page[0]...)

	return result
}

func rawToPNG(r io.Reader, w io.Writer) {
	W := 768
	H := 512
	m := image.NewGray(image.Rect(0, 0, W, H))
	for col := 0; col < W; col += 8 {
		b := make([]byte, H)
		n, err := r.Read(b)
		_ = err
		if n == 0 {
			break
		}
		for row := 0; row < n; row += 1 {
			TransferByte(m, col, row, b[row])
		}
	}

	err := png.Encode(w, m)
	if err != nil {
		log.Fatal(err)
	}
}

func fontToPNG(r io.Reader, w io.Writer) {
	W := 256
	H := 24
	m := image.NewGray(image.Rect(0, 0, W, H))

	for row := 0; row < H; row += 8 {
		b := make([]byte, W)
		n, err := r.Read(b)
		fmt.Println(row, n)
		_ = err
		if n == 0 {
			break
		}
		// There is some confusion here, relying on the fact
		// that a glyph is 8 pixels wide and also 8 bytes high.
		for col := 0; col < n; col += 8 {
			for r := 0; r < 8; r += 1 {
				TransferByte(m, col, row+r, b[col+r])
			}
		}
	}

	err := png.Encode(w, m)
	if err != nil {
		log.Fatal(err)
	}
}

// Transfer the 8 bits of a byte into the 8 pixels starting
// at [col, row].
func TransferByte(i *image.Gray, col, row int, b byte) {
	for bit := 0; bit < 8; bit += 1 {
		i.Set(col+7-bit, row, color.Gray{255 * ((b >> bit) & 1)})
	}

}
