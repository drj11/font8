# Font 8

Extract low-resolution fonts from 8-bit video games.

Typically these will be bi-level 8x8 letter forms.

## Install

Typical setup using Dun Darach.

Download Dun Darach: DunDarach.tzx.zip from
https://www.worldofspectrum.org/infoseekid.cgi?id=0001538

unzip to extract the `.tzx` file.
At the moment we have to load this into an emulator and save a
snapshot.

```
fuse 48.rom Dun\ Darach.txz
```

Will load game.
You may have to be in same directory as 48.rom for this to work.

Save a snapshot (F2, or select using menu on F1) in `.szx` format.
According to the manual the snapshot is saved in `.szx` format,
or `.z80` or `.sna` if you use that extension.

run `snaptopng` on the snapshot.

If the result PNG is called `dun.png` you can open `index.html`
in a browser.
It allows you to select a suitable portion of the memory to
extract.

With a suitable byte offset in hand, run
`snaptopng -offset 48640 game.szx`
to generate a 256 x 24 PNG with 96 8x8 glyphs in.
In this case the output PNG file will be called `game-font.png`.

## Components

snaptopng - convert SZX snapshot into PNG

pngbrowse - interactive HTML tool to find suitable areas of ROM

romtrim - convert selected ROM to PNG font

pngtottf - convert PNG to True Type Font (or similar)

## snaptopng

Written in Go.

This produces a PNG image from a SZX snapshot.
It has two modes: _full_ and _subset_.
In _full_ mode
the output is a 768 x 512 pixel PNG of the full 48KiB snapshot.
In _subset_ mode, when the `-offset` flag is used,
the output is a 256 x 24 pixel PNG of
the 96 8x8 glyphs found at that byte offset.

In ZX Spectrum format, each glyph is an 8x8 pixel image.
Each 8 pixel row is a single byte (bit 0 on right).
One glyph is 8 bytes with the first byte being the top row.

The full mode ouput (768x512) is intended to be used by the
`public/index.html` browser to identify a suitable subset.

I think in the ideal world full mode would produce
a PNG that was 8x49152 pixels;
that would be simple to manipulate using CSS and JavaScript.
But that exceeds that maximum height of a PNG image.

So `snaptopng` full mode creates a PNG that is 512 pixels high.
The leftmost 8-pixel-wide strip form the first 512 bytes
of the ZX Spectrum RAM.
And so on going from left to right.

The full mode output PNG is 768 pixels wide by 512 pixels high
(48Kibibytes).

The subset mode output is intended
to conveniently display up to 96 glyphs in three rows of 32
(conventionally, the ASCII printable sequence from 0x20 to 0x7f,
including DEL for 0x7f, which is printable on the ZX Spectrum).

The subset mode output PNG is 256 pixels wide by 24 pixels high.

